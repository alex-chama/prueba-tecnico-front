# Conjunto de usuarios al azar

Creado con Vue CLI 3 y biblioteca Axxios.
## Empezando
Estas instrucciones le permitirán obtener una copia del proyecto y lograr visualizarlo en su máquina local con motivos de desarrollo o de prueba. 

### Prerequisitos
　*　Tener instalado yarn
　*　Tener instalado Vue CLI 3
　
### Instalando proyecto

Antes de correr
```
yarn install
```
Es necesario ingresar a la carpeta pruebafront, y desde esta ubicación proceder a su instalación.

Una vez finalizado, puede correr
```
vue ui
```
Que le abrirá una ventana con el gestor de proyectos mediante una interfaz amigable.

No olvide importar el proyecto para seleccionar y darle uso.

## Authors
* **V. Alex Espinosa Rivera** - *Front-End Developer* - [V-Alex](https://github.com/v-alex)





